# `sylva-logging-flows` Helm chart

## Purpose

The key role of this chart is to automate the creation of `Flow.logging.banzaicloud.io` and `ClusterFlow.logging.banzaicloud.io` resources via GitOps workflows.

### Flow and ClusterFlow

Flow and ClusterFlow define which logs to collect and filter and which output to send the logs to.
Flow resources are namespaced, the selector only select Pod logs within namespace. ClusterFlow defines a Flow without namespace restrictions. It is also only effective in the controlNamespace. ClusterFlow selects logs from ALL namespace.

### Output and ClusterOutput

Outputs are the destinations where your log forwarder sends the log messages.

ClusterOutput defines an Output without namespace restrictions. It is only evaluated in the controlNamespace by default unless allowClusterResourcesFromAllNamespaces is set to true

Note: Flow can be connected to Output and ClusterOutput, but ClusterFlow can be attached only to ClusterOutput.

## Configuration

The following table lists the configurable parameters of the ancher-users-resources chart and their default values.

| Parameter                | Description             | Default        |
| ------------------------ | ----------------------- | -------------- |
| `flows` | Set of cluster flows to be created.  | `{}` |
| `flows.x.namespace` | The namespace on which the clusterflow is applied | `""` |
| `flows.x.flowLabel` | The clusterflow label  | `""` | 
| `flows.x.globalOutputRefs` | Set of ClusterOutputs  | `[]` |
| `flows.x.localOutputRefs` | Set of Outputs  | `[]` |
| `flows.x.loggingRef` | Reference to the logging system. Each of the loggingRefs can manage a fluentbit daemonset and a fluentd statefulset. (optional)  | `""` |
| `flows.x.match` | Set of filters to be match (optional) | `[]` |
| `flows.x.match[].select` | Set of filters to be match (optional) | `""` |
| `flows.x.match[].select.container_names` | List of matching containers (not Pods) | `""` |
| `flows.x.match[].select.hosts` | List of matching hosts | `""` |
| `flows.x.match[].select.labels` | Key - Value pairs of labels | `""` |
| `flows.x.match[].exclude` | Set of filters to be match (optional) | `""` |
| `flows.x.match[].exclude.container_names` | List of matching containers (not Pods) | `""` |
| `flows.x.match[].exclude.hosts` | List of matching hosts | `""` |
| `flows.x.match[].exclude.labels` | Key - Value pairs of labels | `""` |
| `flows.x.filters` | Set of filters to be created (optional) for complet list of available filters and their description see kube-logging corresponding documentation (https://kube-logging.dev/docs/configuration/plugins/filters/) | `""` |
| `clusterflows` | Set of cluster flows to be created.  | `{}` |
| `clusterflows.x.namespace` | The namespace on which the clusterflow is applied | `""` |
| `clusterflows.x.flowLabel` | The clusterflow label  | `""` |
| `clusterflows.x.globalOutputRefs` | Set of ClusterOutputs  | `[]` |
| `clusterflows.x.loggingRef` | Reference to the logging system. Each of the loggingRefs can manage a fluentbit daemonset and a fluentd statefulset. (optional)  | `""` |
| `clusterflows.x.match` | Set of filters to be match (optional) | `[]` |
| `clusterflows.x.match[].select` | Set of filters to be match (optional) | `""` |
| `clusterflows.x.match[].select.container_names` | List of matching containers (not Pods) | `""` |
| `clusterflows.x.match[].select.hosts` | List of matching hosts | `""` |
| `clusterflows.x.match[].select.labels` | Key - Value pairs of labels | `""` |
| `clusterflows.x.match[].exclude` | Set of filters to be match (optional) | `""` |
| `clusterflows.x.match[].exclude.container_names` | List of matching containers (not Pods) | `""` |
| `clusterflows.x.match[].exclude.hosts` | List of matching hosts | `""` |
| `clusterflows.x.match[].exclude.labels` | Key - Value pairs of labels | `""` |
| `clusterflows.x.filters` | Set of filters to be created (optional) for complet list of available filters and their description see kube-logging corresponding documentation (https://kube-logging.dev/docs/configuration/plugins/filters/) | `""` |
| `outputs` | Set of outputs to be created. For now only syslog output type is supported  | `{}` |
| `outputs.x.syslog.host` | Destination host address | `""` |
| `outputs.x.syslog.allow_self_signed_cert` | allow_self_signed_cert for mutual tls (optional) | `false` |
| `outputs.x.syslog.buffer` | The buffer setup. see https://kube-logging.dev/docs/configuration/plugins/outputs/buffer/ for detail description and parameters (optional) | `""` |
| `outputs.x.syslog.client_cert_path` | file path for private_key_path (optional) | `""` |
| `outputs.x.syslog.enable_system_cert_store` | cert_store to set ca_certificate for ssl context (optional) | `""` |
| `outputs.x.syslog.format` | format rcr5424 use for the logs see https://kube-logging.dev/docs/configuration/plugins/outputs/format_rfc5424/ for detail description  (optional) | `""` |
| `outputs.x.syslog.fqdn` | Fqdn | `nil` |
| `outputs.x.syslog.insecure` | skip ssl validation (optional) | `false` |
| `outputs.x.syslog.port` | Destination host port (optional) | `514` |
| `outputs.x.syslog.private_key_passphrase` | PrivateKeyPassphrase for private key (optional) | `nil` |
| `outputs.x.syslog.private_key_path` | file path for private_key_path (optional) | `""` |
| `outputs.x.syslog.slow_flush_log_threshold` | The threshold for chunk flush performance check. Parameter type is float, not time, default: 20.0 (seconds) If chunk flush takes longer time than this threshold, fluentd logs warning message and increases metric fluentd_output_status_slow_flush_count. (optional) | `""` |
| `outputs.x.syslog.transport` | Transport Protocol (optional) | `"tls"` |
| `outputs.x.syslog.trusted_ca_path` | file path to ca to trust (optional) | `""` |
| `outputs.x.syslog.version` | TLS Version (optional) | `"TLSv1_2"` |
| `clusteroutputs` | Set of cluster outputs to be created. For now only syslog output type is supported  (optional) | `{}` |
| `clusteroutputs.x.syslog.host` | Destination host address (optional) | `""` |
| `clusteroutputs.x.syslog.enabledNamespaces` | list of additional namespace to enable this output | `""` |
| `clusteroutputs.x.syslog.allow_self_signed_cert` | allow_self_signed_cert for mutual tls (optional) | `false` |
| `clusteroutputs.x.syslog.buffer` | The buffer setup. see https://kube-logging.dev/docs/configuration/plugins/outputs/buffer/ for detail description and parameters (optional) | `""` |
| `clusteroutputs.x.syslog.client_cert_path` | file path for private_key_path (optional) | `""` |
| `clusteroutputs.x.syslog.enable_system_cert_store` | cert_store to set ca_certificate for ssl context (optional) | `""` |
| `clusteroutputs.x.syslog.format` | format rcr5424 use for the logs see https://kube-logging.dev/docs/configuration/plugins/outputs/format_rfc5424/ for detail description  (optional) | `""` |
| `clusteroutputs.x.syslog.fqdn` | Fqdn (optional) | `nil` |
| `clusteroutputs.x.syslog.insecure` | skip ssl validation (optional) | `false` |
| `clusteroutputs.x.syslog.port` | Destination host port (optional) | `514` |
| `clusteroutputs.x.syslog.private_key_passphrase` | PrivateKeyPassphrase for private key (optional) | `nil` |
| `clusteroutputs.x.syslog.private_key_path` | file path for private_key_path (optional) | `""` |
| `clusteroutputs.x.syslog.slow_flush_log_threshold` | The threshold for chunk flush performance check. Parameter type is float, not time, default: 20.0 (seconds) If chunk flush takes longer time than this threshold, fluentd logs warning message and increases metric fluentd_output_status_slow_flush_count. (optional) | `""` |
| `clusteroutputs.x.syslog.transport` | Transport Protocol (optional) | `"tls"` |
| `clusteroutputs.x.syslog.trusted_ca_path` | file path to ca to trust (optional) | `""` |
| `clusteroutputs.x.syslog.version` | TLS Version (optional) | `"TLSv1_2"` |
